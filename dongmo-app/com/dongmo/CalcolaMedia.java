package dongmo;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import dongmo.model.AccessOrari;

public class CalcolaMedia {
	public float getMedia(String filepath,String selected) throws FileNotFoundException {
		ReadFromFile reader = new ReadFromFile();
		 List<AccessOrari> acc = new ArrayList<AccessOrari>();
		 acc = reader.readCsv(filepath, selected);
		int totale = 0;
		int number =0;
		while(number!=acc.size()) {
			totale =totale + acc.get(number).getTotale();
			number++;
		}
		//System.out.println("numero riga selezionati ="+ number + " Totale sommato = "+ totale);
		return totale/number;
		
	}
}
