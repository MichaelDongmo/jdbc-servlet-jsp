package dongmo.factory;

public class ProductFactory {

	public PlaceType getPlaceType(String place) throws Exception {
		PlaceType type1=null;
		switch (place) {
		case "db":
			type1 = new DbSaveProduct();
			break;
		case "csv":
			type1 = new CsvSaveProd();
			break;
		case "csvMultipli":
			type1 =  new CsvMultipliSaveProd();
			break;
		default:
			throw new Exception("no data type found");
		}
		return type1;
	}
}
