package dongmo.factory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import dongmo.model.Prodotto;

public class CsvMultipleWriter {
	public void printProductList(List<Prodotto> p,String file, String[] header) throws IOException{
        FileWriter pw = new FileWriter(file,true);
        Iterator<Prodotto> s = p.iterator();
        int i=0;
        if (s.hasNext()==false){
            System.out.println("Empty");
        }
        while(s.hasNext()){
            Prodotto current  = (Prodotto) s.next();
            String[] actual = current.getMultiProd().split(";");
            //System.out.println(current.toString()+"\n");
            while(i<actual.length) {
            	 pw.append(actual[i]);
                 pw.append("\n");
                 i++;
            }
        }
            pw.flush();
            pw.close();
    }

}
