package dongmo.factory;

import java.util.ArrayList;
import java.util.List;

import dongmo.csv.CsvDb;
import dongmo.model.Prodotto;

public class CsvSaveAnag implements PlaceType{

	private static final String anag_file = "/Users/momog/Desktop/jdbc-servlet-jsp/dongmo-app/anagrafica.csv";
	private static String[] header = { "Product_Id", "Prodcut_Name", "Product_Price", "Date_Ins" };
	
	@Override
	public void save(Prodotto prodotto) {
		List<Prodotto> list = new ArrayList<>();
		CsvDb csvDb = new CsvDb();
		try {
			list.add(prodotto);
			csvDb.create(anag_file, header, list);
		} catch (Exception e) {
			System.out.print(e);
		}	
	}
}
