package dongmo.factory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dongmo.connectionDB.ConnectDB;
import dongmo.model.Prodotto;
import dongmo.singleton.EagerConnectDbSingl;

public class DbSaveProduct implements  PlaceType {

	@SuppressWarnings("unused")
	public void save(Prodotto p) {

		//ConnectDB con = new ConnectDB();
		EagerConnectDbSingl con1 = EagerConnectDbSingl .getInstance();
		try {
			//Connection connect = con.connection();
			Connection connect = con1.getCon();
			Statement cmd = connect.createStatement();
			if (p.getDataType().compareTo("Prodotto") == 0 || p.getDataType().compareTo("Anagrafica") == 0) {
				String query = " insert into dati_inseriti(supplier_id,data_name,data_import,data_operation)"
						+ "values ('" + p.getId() + "', '" + p.getName() + "', '" + p.getImporto() + "', '"
						+ p.getDataIns() + "')";

				ResultSet res1 = cmd.executeQuery(query);
			} else {
				String query = " insert into movimenti(supplier_id,data_name,data_import,data_operation)" + "values ('"
						+ p.getId() + "', '" + p.getName() + "', '" + p.getImporto() + "', '" +  p.getDataIns() + "')";

				ResultSet res1 = cmd.executeQuery(query);
			}

			cmd.close();
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
