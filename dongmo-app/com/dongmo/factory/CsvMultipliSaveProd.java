package dongmo.factory;

import java.util.ArrayList;
import java.util.List;

import dongmo.model.Prodotto;

public class CsvMultipliSaveProd implements PlaceType{

	private static final String multi_prod_file = "/Users/momog/Desktop/jdbc-servlet-jsp/dongmo-app/prodottoMultipli.csv";
	private static final String multi_anag_file = "/Users/momog/Desktop/jdbc-servlet-jsp/dongmo-app/anagraficaMultipli.csv";
	private static String[] header = { "Product_Id", "Prodcut_Name", "Product_Price", "Date_Ins" };

	@Override
	public void save(Prodotto p) {
		List<Prodotto> list = new ArrayList<>();
		CsvMultipleWriter csvMultipleWriter = new CsvMultipleWriter();
		try {
			if (p.getDataType().compareTo("Prodotto") == 0) {
				list.add(p);
				csvMultipleWriter.printProductList(list, multi_prod_file,header);
			} else if (p.getDataType().compareTo("Anagrafica") == 0) {
				list.add(p);
				csvMultipleWriter.printProductList( list,multi_anag_file,header);
			}

		} catch (Exception e) {
			System.out.print(e);
		}

		
	}

}
