package dongmo.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Prodotto implements Serializable{

	private String place;
	private String dataType;
	private int id;
	private String name;
	private int importo;
	private String dataIns;
	private String MultiProd;


	public String getPlace() {
		return place;
	}

	public String getMultiProd() {
		return MultiProd;
	}

	public void setMultiProd(String multiProd) {
		MultiProd = multiProd;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getId() {
		return id;
	}
	
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getImporto() {
		return importo;
	}

	public void setImporto(int importo) {
		this.importo = importo;
	}

	public String getDataIns() {
		return dataIns;
	}

	public void setDataIns(String dataIns) {
		this.dataIns = dataIns;
	}

	public Prodotto() {
		super();
	}

	@Override
	public String toString() {
		return "Prodotto [place=" + place + ", dataType=" + dataType + ", id=" + id + ", name=" + name + ", importo="
				+ importo + ", dataIns=" + dataIns + "]";
	}

}
