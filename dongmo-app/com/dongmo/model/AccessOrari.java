package dongmo.model;

public class AccessOrari {
	private String data;
	private String timeIndex;
	private int totale;
	private int areac;
	
	
	
	
	public AccessOrari(String data, String timeIndex, int totale, int areac) {
		super();
		this.data = data;
		this.timeIndex = timeIndex;
		this.totale = totale;
		this.areac = areac;
	}
	public AccessOrari() {
		// TODO Auto-generated constructor stub
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getTimeIndex() {
		return timeIndex;
	}
	public void setTimeIndex(String timeIndex) {
		this.timeIndex = timeIndex;
	}
	public int getTotale() {
		return totale;
	}
	public void setTotale(int totale) {
		this.totale = totale;
	}
	public int getAreac() {
		return areac;
	}
	public void setAreac(int areac) {
		this.areac = areac;
	}
	@Override
	public String toString() {
		return "AccessOrari [data=" + data + ", timeIndex=" + timeIndex + ", totale=" + totale + ", areac=" + areac
				+ "]";
	}

}
