package dongmo.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import dongmo.model.Prodotto;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CsvDb {

		public void create(String inputFile, String[] header, List<Prodotto> list) throws IOException {
			try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(inputFile))) {
				try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT.withHeader(header))) {
					for (Prodotto n : list) {
						csvPrinter.printRecord("\n");
						csvPrinter.printRecord(n.getId(), n.getName(), n.getImporto(), n.getDataIns());
					}
					csvPrinter.flush();
				}
			}
		}

}
