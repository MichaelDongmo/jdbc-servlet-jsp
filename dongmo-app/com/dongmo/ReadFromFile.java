package dongmo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dongmo.model.AccessOrari;

public class ReadFromFile {
	@SuppressWarnings("resource")
	public List<AccessOrari> readCsv(String filepath, String entry) throws FileNotFoundException {
		List<AccessOrari> lista = new ArrayList<AccessOrari>();
		AccessOrari acc;
		// pass the path to the file as a parameter
		File file = new File(filepath);
		Scanner sc = new Scanner(file);
		String[] riga = null;
		while (sc.hasNextLine()) {
			riga = sc.nextLine().split(";");
			//System.out.println(sc.nextLine());
			if(riga[0].compareTo(entry)==0) {
				acc=createBook(riga);
				lista.add(acc);
			}
		}
		return lista;
	}
	
	private static AccessOrari createBook(String[] el) {
		String data = el[0];
		String timeIndex = el[1];
		int totale = Integer.parseInt(el[2]);
		int areac = Integer.parseInt(el[3]);

		// create and return book of this metadata
		return new AccessOrari(data, timeIndex, totale, areac);
	}
}
