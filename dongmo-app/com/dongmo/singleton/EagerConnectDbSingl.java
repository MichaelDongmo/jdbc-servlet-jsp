package dongmo.singleton;

/*DBConnection: Eager Initialization Method*/

import java.sql.Connection;
import java.sql.DriverManager;

public class EagerConnectDbSingl {
	private static final EagerConnectDbSingl instance = new EagerConnectDbSingl();
	
	private Connection connection;
	private String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	private String user = "hr";
	private String password = "hr";
	
	public EagerConnectDbSingl(){
		try {
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				this.connection=DriverManager.getConnection(url, user, password);
			} catch (Exception e) {
				System.out.println("Database Connection Creation Failed : " + e.getMessage());
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public Connection getCon(){
		return connection;
	}
	
	
	public static EagerConnectDbSingl getInstance() {
		return instance;
	}

}
