package dongmo.singleton;

/*DBConnection: Lazy Initialization with double lock Method*/

import java.sql.Connection;
import java.sql.DriverManager;

public class LazyConnectDbSingl {

	private Connection connection;
	private String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	private String user = "hr";
	private String password = "hr";

	private static LazyConnectDbSingl SINGLE_INSTANCE = null;

	private LazyConnectDbSingl() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			this.connection = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			System.out.println("Database Connection Creation Failed : " + e.getMessage());
		}
	}
	
	public Connection getConnection() {
		return this.connection;
	}

	public static LazyConnectDbSingl getInstance() {
		if (SINGLE_INSTANCE == null) {

			synchronized (LazyConnectDbSingl.class) {

				SINGLE_INSTANCE = new LazyConnectDbSingl();

			}

		}

		return SINGLE_INSTANCE;
	}
}
