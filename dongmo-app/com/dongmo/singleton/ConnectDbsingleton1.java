package dongmo.singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDbsingleton1 {
	private static ConnectDbsingleton1 singletonInstance ;

	private Connection connection;
	private String url = "jdbc:oracle:thin:@localhost:1521:orcl";
	private String user = "hr";
	private String password = "hr";

	private ConnectDbsingleton1(){
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			this.connection=DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			System.out.println("Database Connection Creation Failed : " + e.getMessage());
		}
	}
	
	public Connection getCon(){
		return connection;
	}

	public static ConnectDbsingleton1 getInstance(){
		if(singletonInstance == null) {
			singletonInstance = new ConnectDbsingleton1();
		} else
			try {
				if(singletonInstance.getCon().isClosed()) {
					singletonInstance = new ConnectDbsingleton1();
				}
			} catch (SQLException e) {
				e.getMessage();
			}
		return singletonInstance;
	}

}
