package dongmo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dongmo.csv.StringController;
import dongmo.factory.PlaceType;
import dongmo.factory.ProductFactory;
import dongmo.model.Prodotto;

@SuppressWarnings("serial")
public class Servlet4 extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		PrintWriter pw = resp.getWriter();
		try {
			resp.setContentType("text/html");
			String place = req.getParameter("Place");
			String datatype = req.getParameter("tipoDiDato");

			String name = req.getParameter("name");
			String date = req.getParameter("dataOperazione");
			int importo = Integer.parseInt(req.getParameter("importo"));
			String multiProduct = req.getParameter("multiproduct");

			String error = "Error.jsp";
			String success = "Success.jsp";
			String errorId = "ErrorId.jsp";
			
			RequestDispatcher rd1 = req.getRequestDispatcher(error);
			RequestDispatcher rd2 = req.getRequestDispatcher(success);
			RequestDispatcher rd3 = req.getRequestDispatcher(errorId);
			
			int prodId;
			try {
				prodId = Integer.parseInt(req.getParameter("id"));
			} catch (Exception e) {
				pw.print("si è verificato un errore sul campo id. Formato del valore " + req.getParameter("id")
						+ " non valido");
				return;
			}

			if (place.compareTo("csv") == 0 && datatype.compareTo("Movimento") == 0
					|| place.compareTo("csvMultipli") == 0 && datatype.compareTo("Movimento") == 0) {
				
				rd3.forward(req, resp);
//				pw.print("<html>");
//				pw.print("<body>");
//				pw.print("<center>");
//				pw.print("<font size=5 color=red>" + "<h2>Scelta non consentita...!<h2></font>");
//				pw.print("</body>");
//				pw.print("</center>");
//				pw.print("</body>");
//				pw.print("</html>");
			} else {

				Prodotto p = new Prodotto();
				p.setId(prodId);
				p.setName(name);
				p.setImporto(importo);
				p.setDataIns(date);
				p.setDataType(datatype);
				p.setMultiProd(multiProduct);
				// pf.saveProduct(place);

				// Controllo stringa multipla
				StringController stringController = new StringController();
				int dim = stringController.stringController(multiProduct);
				if (dim == 4) {
					ProductFactory pf = new ProductFactory();
					PlaceType target = pf.getPlaceType(place);
					target.save(p);

					rd1.forward(req, resp);
					
//					pw.print("<html>");
//					pw.print("<center>");
//					pw.print("<font size=5 color=green>" + "<h2>Inserimento avvenuto con successo...!<h2></font>");
//					pw.print("</body>");
//					pw.print("</center>");
//					pw.print("</html>");
				} else {
					rd2.forward(req, resp);
					
//					pw.print("<html>");
//					pw.print("<center>");
//					pw.print("<font size=5 color=red>" + "<h2>Controllare il campo MultiProduct...!<h2></font>");
//					pw.print("</body>");
//					pw.print("</center>");
//					pw.print("</html>");
				}

			}
		} catch (Exception e) {
			pw.print(e);
		}

	}

}
