package dongmo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dongmo.model.AccessOrari;

@SuppressWarnings("serial")
public class Servlet5 extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String prod_file = "/Users/momog/Desktop/jdbc-servlet-jsp/dongmo-app/v2_accessiorari_areacundefined_2012.csv";
		resp.setContentType("text/html");
		String active = req.getParameter("active");
		if(active == null) {
			active = "";
		}
		String forwardTo = "forward.jsp";
		String error = "checkbox.jsp";
		try {
			String date = req.getParameter("dataOperazione");
			ReadFromFile readfile = new ReadFromFile();
			List<AccessOrari> element = new ArrayList<>();
			element = readfile.readCsv(prod_file, date);
			float media = 0;

			RequestDispatcher requestDispatcher;// = req.getRequestDispatcher(forwardTo);
			// Read element and forwad to forward.jsp page
			element = readfile.readCsv(prod_file, date);
			req.setAttribute("data", element);
			// requestDispatcher.forward(req, resp);
			// Control if the checkbox is on and send data and media to user
			switch (active) {
			case "ON":
				CalcolaMedia calcola = new CalcolaMedia();
				media = calcola.getMedia(prod_file, date);
				requestDispatcher = req.getRequestDispatcher(forwardTo);
				req.setAttribute("media", media);
				requestDispatcher.forward(req, resp);
				break;
			case "":
				RequestDispatcher req1 = req.getRequestDispatcher(error);
				req1.forward(req, resp);
				break;

			default:
				break;
			}
//			
//			if (active.compareTo("ON") == 0) {
//				CalcolaMedia calcola = new CalcolaMedia();
//				media = calcola.getMedia(prod_file, date);
//				requestDispatcher = req.getRequestDispatcher(forwardTo);
//				req.setAttribute("media", media);
//				requestDispatcher.forward(req, resp);
//			} else{
//				CalcolaMedia calcola = new CalcolaMedia();
//				media = calcola.getMedia(prod_file, date);
//				RequestDispatcher req1 = req.getRequestDispatcher(error);
//				req.setAttribute("media", media);
//				req1.forward(req, resp);
//			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
