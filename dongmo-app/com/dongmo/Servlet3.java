package dongmo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dongmo.model.Prodotto;
import dongmo.strategy.SalvaProdotto;

@SuppressWarnings("serial")
public class Servlet3 extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		try {
			String place = req.getParameter("Place");
			String datatype = req.getParameter("tipoDiDato");

			String name = req.getParameter("name");
			String date = req.getParameter("dataOperazione");
			int importo = Integer.parseInt(req.getParameter("importo"));

			int prodId;
			try {
				prodId = Integer.parseInt(req.getParameter("id"));
			} catch (Exception e) {
				pw.print("si è verificato un errore sul campo id. Formato del valore " + req.getParameter("id")
						+ " non valido");
				return;
			}

			if (place.compareTo("csv") == 0 && datatype.compareTo("Movimento") == 0) {
				pw.print("<html>");
				pw.print("<body>");
				pw.print("<center>");
				pw.print("<font size=5 color=red>"+"<h2>Scelta non consentita...!<h2></font>");
				pw.print("</body>");
				pw.print("</center>");
				pw.print("</body>");
				pw.print("</html>");
			} else {

				Prodotto p = new Prodotto();
				p.setId(prodId);
				p.setName(name);
				p.setImporto(importo);
				p.setDataIns(date);
				p.setDataType(datatype);
				p.setPlace(place);

				// save Prodotto

				SalvaProdotto sp = new SalvaProdotto();
				sp.SaveProd(p);

				pw.print("<html>");
				pw.print("<body>");
				pw.print("<center>");
				pw.print("<font size=5 color=green>"+"<h2>Inserimento avvenuto con successo...!<h2></font>");
				pw.print("</body>");
				pw.print("</center>");
				pw.print("</body>");
				pw.print("</html>");
			}
		} catch (Exception e) {
			pw.print(e);
		}
	}
}
