package dongmo.strategy;

import dongmo.model.Prodotto;

public interface StrategySave {
	public void save(Prodotto p);

}
