package dongmo.strategy;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dongmo.connectionDB.ConnectDB;
import dongmo.model.Prodotto;
import dongmo.singleton.LazyConnectDbSingl;

public class StrategyDbSave implements StrategySave {

	@SuppressWarnings("unused")
	@Override
	public void save(Prodotto p) {
		//ConnectDB con = new ConnectDB();
		LazyConnectDbSingl connection = LazyConnectDbSingl.getInstance();
		try {
			Connection connect = connection.getConnection();
			Statement cmd = connect.createStatement();
			if (p.getDataType().compareTo("Prodotto") == 0 || p.getDataType().compareTo("Anagrafica") == 0) {
				String query = " insert into dati_inseriti(supplier_id,data_name,data_import,data_operation)"
						+ "values ('" + p.getId() + "', '" + p.getName() + "', '" + p.getImporto() + "', '"
						+ p.getDataIns() + "')";

				ResultSet res1 = cmd.executeQuery(query);
			} else {
				String query = " insert into movimenti(supplier_id,data_name,data_import,data_operation)" + "values ('"
						+ p.getId() + "', '" + p.getName() + "', '" + p.getImporto() + "', '" + p.getDataIns() + "')";

				ResultSet res1 = cmd.executeQuery(query);
			}

			cmd.close();
			connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
