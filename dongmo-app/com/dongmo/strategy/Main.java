package dongmo.strategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dongmo.CalcolaMedia;
import dongmo.ReadFromFile;
import dongmo.model.AccessOrari;

public class Main {

	public static void main(String[] args) throws IOException {
		String prod_file = "/Users/momog/Desktop/jdbc-servlet-jsp/dongmo-app/v2_accessiorari_areacundefined_2012.csv";
		ReadFromFile readfile = new ReadFromFile();
		List<AccessOrari> element = new ArrayList<>();

		element = readfile.readCsv(prod_file,"2012-01-03");
		int i = 0;
		while (i != element.size()) {
			System.out.print("data = "+element.get(i).getData() + " OraAccesso= "+element.get(i).getTimeIndex()
					+ "Totale ="+element.get(i).getTotale() +" Area = "+element.get(i).getAreac()+"\n");
			i++;
		}
		
		//get Media totale
		
		CalcolaMedia calcola = new CalcolaMedia();
		float media = calcola.getMedia(prod_file,"2012-01-03");
		System.out.println("La media è = " +media);

	}

}
