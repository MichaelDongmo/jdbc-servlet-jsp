package dongmo.strategy;

import java.util.ArrayList;
import java.util.List;

import dongmo.csv.CsvDb;
import dongmo.model.Prodotto;

public class CsvFileSave implements StrategySave {
	private static final String prod_file = "/Users/momog/Desktop/dongmo-app/prodotto.csv";
	private static final String anag_file = "/Users/momog/Desktop/dongmo-app/anagrafica.csv";
	private static String[] header = { "Product_Id", "Prodcut_Name", "Product_Price", "Date_Ins" };
	List<Prodotto> list = new ArrayList<>();
	private CsvDb csvDb = new CsvDb();
	@Override
	public void save(Prodotto p) {
		try {
			if (p.getDataType().compareTo("Prodotto") == 0) {
				list.add(p);
				csvDb.create(prod_file, header, list);
			} else if (p.getDataType().compareTo("Anagrafica") == 0) {
				list.add(p);
				csvDb.create(anag_file, header, list);
			}

		} catch (Exception e) {
			System.out.print(e);
		}
		
	}

}
