package dongmo.strategy;

import dongmo.model.Prodotto;

public class SalvaProdotto {
	public void SaveProd(Prodotto p) {
		switch (p.getPlace()) {
		case "db":
			ProductStrategy stg = new ProductStrategy(new StrategyDbSave());
			stg.executeStrategy(p);
			break;
		case "csv":
			ProductStrategy stg1 = new ProductStrategy(new CsvFileSave());
			stg1.executeStrategy(p);
			break;
		default:
			System.out.println("Campi non correttamente valorizzati...!");
			break;
		}
	}
}
