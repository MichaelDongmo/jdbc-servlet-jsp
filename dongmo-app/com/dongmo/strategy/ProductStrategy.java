package dongmo.strategy;

import dongmo.model.Prodotto;

public class ProductStrategy {
	private StrategySave strategy;

	public ProductStrategy( StrategySave dbSaveProduct) {
		this.strategy = dbSaveProduct;
	}

	public void executeStrategy(Prodotto p) {
		strategy.save(p);
	}
}
