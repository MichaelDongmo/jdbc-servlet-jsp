package dongmo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dongmo.connectionDB.ConnectDB;
import dongmo.csv.CsvDb;
import dongmo.model.Prodotto;
import dongmo.singleton.ConnectDbsingleton1;

@SuppressWarnings("serial")
public class Servlet1 extends HttpServlet {
	private static final String prod_file = "/Users/momog/Desktop/dongmo-app/prodotto.csv";
	private static final String anag_file = "/Users/momog/Desktop/dongmo-app/anagrafica.csv";
	private static String[] header = { "Product_Id", "Prodcut_Name", "Product_Price", "Date_Ins" };

	//private ConnectDB con = new ConnectDB();
	
	private Prodotto prodotto;
	List<Prodotto> list = new ArrayList<>();
	private CsvDb csvDb = new CsvDb();

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			resp.setContentType("text/html");
			PrintWriter pw = resp.getWriter();
			String place = req.getParameter("Place");
			String data_type = req.getParameter("tipoDiDato");

			String name = req.getParameter("name");
			String date = req.getParameter("dataOperazione");
			int importo = Integer.parseInt(req.getParameter("importo"));// Float.parseFloat(req.getParameter("importo"));

			int prodId;
			try {
				prodId = Integer.parseInt(req.getParameter("id"));
			} catch (Exception e) {
				pw.print("si è verificato un errore sul campo id. Formato del valore " + req.getParameter("id")
						+ " non valido");
				return;
			}

			// insert dati nel db utente hr
			ConnectDbsingleton1 con1 = ConnectDbsingleton1.getInstance();
			if (place.compareTo("db") == 0) {
				switch (data_type) {
				case "Prodotto":
				case "Anagrafica":
					try {
						prodId = Integer.parseInt(req.getParameter("id"));
					} catch (Exception e) {
						pw.print("si è verificato un errore sul campo id. Formato del valore " + req.getParameter("id")
								+ " non valido");
						return;
					}
					//Connection connect = con.connection();					
					Connection connect = con1.getCon();
					Statement cmd = connect.createStatement();
					if (prodId == 0 || name.compareTo("") == 0 || importo == 0 || date.compareTo("") == 0)  {
						pw.print("Controllare i campi inseriti...!");
					} else {
						String query = " insert into dati_inseriti(supplier_id,data_name,data_import,data_operation)"
								+ "values ('" + prodId + "', '" + name + "', '" + importo + "', '" + date + "')";

						ResultSet res1 = cmd.executeQuery(query);

						if (res1 != null) {
							pw.print("<html>");
							pw.print("<body>");
							pw.print("<center>");
							pw.print("<font size=5 color=green>"+"<h2>Inserimento avvenuto con successo...!<h2></font>");
							pw.print("</body>");
							pw.print("</center>");
							pw.print("</body>");
							pw.print("</html>");
						} else {
							pw.print("<html>");
							pw.print("<body>");
							pw.print("<center>");
							pw.print("<font size=5 color=redd>"+"<h2>Inserimento fallita...!<h2></font>");
							pw.print("</body>");
							pw.print("</center>");
							pw.print("</body>");
							pw.print("</html>");
						}
					} 
					cmd.close();
					connect.close();
					break;
				case "Movimento":
					//Connection connect1 = con.connection();
					Connection connect1 = con1.getCon();
					Statement stm = connect1.createStatement();
					if (prodId == 0 || name.compareTo("") == 0 || importo == 0 || date.compareTo("") == 0)  {
						pw.print("Controllare i campi inseriti...!");
					} else {
						String query1 = " insert into movimenti(supplier_id,data_name,data_import,data_operation)"
								+ "values ('" + prodId + "', '" + name + "', '" + importo + "', '" + date + "')";
						ResultSet res = stm.executeQuery(query1);
						if (res != null) {
							pw.print("<html>");
							pw.print("<body>");
							pw.print("<center>");
							pw.print("<font size=5 color=green>"+"<h2>Inserimento avvenuto con successo...!<h2></font>");
							pw.print("</body>");
							pw.print("</center>");
							pw.print("</body>");
							pw.print("</html>");
						} else {
							pw.print("<html>");
							pw.print("<body>");
							pw.print("<h2>Registrazione Fallita...!<h2>");
							pw.print("</body>");
							pw.print("</html>");
						}
					} 
					stm.close();
					connect1.close();
					break;
				default:
					break;
				}
			} else {
				switch (data_type) {
				case "Prodotto":
					// carica la lista
					if  (prodId == 0 || name.compareTo("") == 0 || importo == 0 || date.compareTo("") == 0)  {
						pw.print("Controllare i campi inseriti...!");
					} else {
						prodotto = new Prodotto();
						prodotto.setId(prodId);
						prodotto.setName(name);
						prodotto.setImporto(importo);
						prodotto.setDataIns(date);
						list.add(prodotto);
						csvDb.create(prod_file, header, list);
						pw.print("<html>");
						pw.print("<body>");
						pw.print("<center>");
						pw.print("<font size=5 color=green>"+"<h2>Inserimento avvenuto con successo...!<h2></font>");
						pw.print("</body>");
						pw.print("</center>");
						pw.print("</body>");
						pw.print("</html>");
					}
					break;
				case "Anagrafica":
					// carica la lista
					if (prodId == 0 || name.compareTo("") == 0 || importo == 0 || date.compareTo("") == 0) {
						pw.print("Controllare i campi inseriti...!");
					} else {
						prodotto = new Prodotto();
						prodotto.setId(prodId);
						prodotto.setName(name);
						prodotto.setImporto(importo);
						prodotto.setDataIns(date);
						list.add(prodotto);
						csvDb.create(anag_file, header, list);
						pw.print("<html>");
						pw.print("<body>");
						pw.print("<center>");
						pw.print("<font size=5 color=green>"+"<h2>Inserimento avvenuto con successo...!<h2></font>");
						pw.print("</body>");
						pw.print("</center>");
						pw.print("</body>");
						pw.print("</html>");
					}
					break;
				case "Movimento":
					pw.print("<html>");
					pw.print("<body>");
					pw.print("<center>");
					pw.print("<font size=5 color=red>"+"<h2>Scelta non consentita...!<h2></font>");
					pw.print("</body>");
					pw.print("</center>");
					pw.print("</html>");
					break;

				default:
					break;
				}
			}

		} catch (Exception e) {
			PrintWriter pwr = resp.getWriter();
			pwr.print(e);
			// System.out.print(e);// NO, aggiungere log
		}

	}
}
