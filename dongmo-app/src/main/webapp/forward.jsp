<%@page import="dongmo.model.AccessOrari"%>
<%@page import="dongmo.Servlet5"%>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Access</title>
</head>
<body>
	<center>
		<font size="4" color="Green"><h1>Area 'C' Access</h1></font>
		<h1>

			<table class="table table-striped" border="4">
				<tr>
<!-- 					<th scope="col">#</th> -->
					<th scope="col">Data</th>
					<th scope="col">Ora Accesso</th>
					<th scope="col">Totale</th>
					<th scope="col">Area</th>
				</tr>
				<%-- Fetching the attributes of the request object 
             which was previously set by the servlet  
              "Servlet5.java" 
        --%>
				<%ArrayList<AccessOrari> std =  
            (ArrayList<AccessOrari>)request.getAttribute("data");
        for(AccessOrari s:std){%>
				<%-- Arranging data in tabular form 
        --%>
				<tr>
					<td><%=s.getData()%></td>
					<td><%=s.getTimeIndex()%></td>
					<td><%=s.getTotale()%></td>
					<td><%=s.getAreac()%></td>
				</tr>
				<%}%> 
				<tr><td><b>Media</b></td><td></td><td><b>${media}</b></td><td></td></tr>
			</table>
			</h1>
	</center>
</body>

</html>