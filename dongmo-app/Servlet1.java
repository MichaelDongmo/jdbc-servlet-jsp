import java.io.IOException;
import java.io.PrintWriter;

/*udemy@advancia.it
udemy2018*/
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet1 extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		try {
			PrintWriter pw = resp.getWriter();
			String userId = req.getParameter("userID");
			String jobTitle = req.getParameter("userJobtitle");
		} catch (Exception e) {
			System.out.print(e);
		}
		
	}

}


-- Pass data from servlet to jsp

1- At the server side, set the attribute in the request then forward the request to jsp page as the following:
	
request.setAttribute("name", "Hussein Terek");
request.getRequestDispatcher("home.jsp").forward(request, response);
In jsp, you can access the attribute like:

2- In jsp, you can access the attribute like:
	
<h3> My name is ${name}</h3>
<!-- OR -->
<h3> My name is request.getAttribute("name")</h3>